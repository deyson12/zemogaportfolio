<!DOCTYPE html>

<%@page import="com.javatpoint.dao.PortfolioDao"%>
<%@page import="com.javatpoint.bean.Portfolio,java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

	<link rel="stylesheet" href="css/bootstrap.min.css">   		
   	<script src="js/bootstrap.min.js"></script>   
	<link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet" type="text/css" />
	<title>Portfolio List</title>
</head>
<body>

	<%
		List<Portfolio> portfolioList = PortfolioDao.getAllRecords();
		request.setAttribute("portfolioList", portfolioList);
	%>

	<div class="container">
	
		<h1>Portfolio List</h1>
		<table border="1" width="90%" class="table">
		<tr>
			<th style="background-color: #26c476 !important; color: white;">Id Portfolio</th>
			<th style="background-color: #26c476 !important; color: white;">Description</th>
			<th style="background-color: #26c476 !important; color: white;">Image</th>
			<th style="background-color: #26c476 !important; color: white;">Twitter</th>
			<th style="background-color: #26c476 !important; color: white;">Title</th>
			<th style="background-color: #26c476 !important; color: white;">Action</th>
		</tr>
		<c:forEach items="${portfolioList}" var="portfolio">
			<tr>
				<td>${portfolio.getIdPortFolio()}</td>
				<td>${portfolio.getDescription()}</td>
				<td><img alt="image" src="${portfolio.getImageUrl()}"
					width="50px" height="50px"></td>
				<td>${portfolio.getTwitterUserName()}</td>
				<td>${portfolio.getTitle()}</td>
				<td>
					<a href="portfolio.jsp?idportfolio=${portfolio.getIdPortFolio()}" class="btn btn-success" style="background-color: #26c476 !important;">View</a>
				</td>
			</tr>
		</c:forEach>
	</table>
	</div>
</body>
</html>