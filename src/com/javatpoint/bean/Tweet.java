package com.javatpoint.bean;

public class Tweet {
	
	private String text;
	private Entities entities;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Entities getEntities() {
		return entities;
	}

	public void setEntities(Entities entities) {
		this.entities = entities;
	}

}
