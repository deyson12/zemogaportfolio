package com.javatpoint.bean;

public class Portfolio {
	
	private int idPortFolio;
	private String description;
	private String imageUrl;
	private String twitterUserName;
	private String title;
	
	public int getIdPortFolio() {
		return idPortFolio;
	}
	public void setIdPortFolio(int idPortFolio) {
		this.idPortFolio = idPortFolio;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getTwitterUserName() {
		return twitterUserName;
	}
	public void setTwitterUserName(String twitterUserName) {
		this.twitterUserName = twitterUserName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

}
