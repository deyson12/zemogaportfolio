package com.javatpoint.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.javatpoint.bean.Portfolio;
import com.javatpoint.database.DataBase;

public class PortfolioDao {

	public static List<Portfolio> getAllRecords() {
		List<Portfolio> list = new ArrayList<>();

		try {
			Connection con = DataBase.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from portfolio");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Portfolio portfolio = new Portfolio();
				portfolio.setIdPortFolio(rs.getInt("idportfolio"));
				portfolio.setDescription(rs.getString("description"));
				portfolio.setImageUrl(rs.getString("image_url"));
				portfolio.setTwitterUserName(rs.getString("twitter_user_name"));
				portfolio.setTitle(rs.getString("title"));
				list.add(portfolio);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return list;
	}

	public static Portfolio getRecordById(int id) {
		Portfolio portfolio = null;
		try {
			Connection con = DataBase.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from portfolio where idportfolio=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				portfolio = new Portfolio();
				portfolio.setIdPortFolio(rs.getInt("idportfolio"));
				portfolio.setDescription(rs.getString("description"));
				portfolio.setImageUrl(rs.getString("image_url"));
				portfolio.setTwitterUserName(rs.getString("twitter_user_name"));
				portfolio.setTitle(rs.getString("title"));
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return portfolio;
	}

}
