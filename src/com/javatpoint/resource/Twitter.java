package com.javatpoint.resource;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.javatpoint.bean.Tweet;

public class Twitter {

	public static List<Tweet> getTwitsByUser(String user) {
		HttpURLConnection conn = null;
		Tweet[] tweets = null;
		List<Tweet> tweetsList = new ArrayList<>();
		try {
			URL url = new URL("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name="+user+"&count=5");
			conn = (HttpURLConnection) url.openConnection();
	        conn.setRequestMethod("GET");
	        conn.setRequestProperty("Accept", "application/json");
	        conn.setRequestProperty("Authorization", "Bearer AAAAAAAAAAAAAAAAAAAAAPNkCAEAAAAAmcfv%2BXE59uvAZ6pzjUv%2Fu68sBHI%3DpujwAAAwLsqbNQivymaOn0NveuOmKcRItDb5TUpn8fXY3iUMs6");

	        if (conn.getResponseCode() == 200) {	           

		        BufferedReader br = new BufferedReader(new InputStreamReader(
		            (conn.getInputStream())));
	
		        String output;	        
		        Gson gson = new Gson();
		        
		        while ((output = br.readLine()) != null) {
		        	tweets = gson.fromJson(output, Tweet[].class);
		        }
		        
		        for(Tweet tweet : tweets) {
		        	tweetsList.add(tweet);
		        }
	        
	        }

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
		return tweetsList;

	}

}
